#!/bin/bash

error='The bWAPP database already exists...'
success='bWAPP has been installed successfully!'
file=/tmp/status.log

check_message(){
    message=$1
    file=$2

    if [[ ! -z "$message" || ! -z "$file" ]] ; then
        check=$(egrep -o "$message" "$file")
        if [ "$check" == "$message" ] ; then
            return 1
        else
            return 0
        fi
    else
        return 0
    fi
}

curl -s 'http://localhost/install.php?install=yes' -o "$file"

if [ -f "$file" ] ; then
    if check_message "$success" "$file" ; then
        exit 0
    elif check_message "$error" "$file" ; then
        exit 0
    fi
else
    exit 1
fi
