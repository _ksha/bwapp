# bWAPP

an extremely buggy web app !, this repo contains a port of the original project [bWAPP](https://sourceforge.net/projects/bwapp/files/bWAPP/), but this project it's made to run on docker.

## Getting started

```bash
git clone https://gitlab.com/_ksha/bwapp
cd bwapp
docker-compose up -d
```

## Attack

* Go to the login page. If you browse the bWAPP root directory you will be redirected.

example: <http://localhost/bWAPP/>

* Login with the default credentials, or make a new user.

Default passwords are bee/bug

## Todo

- [x] Fix if database exist
- [x] Fix index redirect
- [ ] Convert apache to nginx image.
- [ ] Remove hardcoded passwords, databases, etc.
- [x] Fix database population.
- [ ] Add SSL Support.
- [ ] Add new vulnerabilities.
